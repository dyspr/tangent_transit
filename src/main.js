var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var steps = 128
var randNums = []

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < steps; i++) {
    randNums.push(Math.random())
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < randNums.length; i++) {
    if (randNums[i] >= 0.5 + 0.25 * sin(frameCount * 0.1)) {
      fill(0)
    } else {
      fill(255)
    }
    noStroke()
    push()
    translate(windowWidth * 0.5 + (i - Math.floor(randNums.length * 0.5) + 0.5) * boardSize * (1 / steps) * 0.85, windowHeight * 0.5)
    rect(0, 0, boardSize * (1 / steps), boardSize * 0.85)
    pop()

    for (var j = 0; j < 3; j++) {
      push()
      translate(0, boardSize * (j - 1) * 0.25)
      if (randNums[i] >= 0.5 + 0.35 * sin(frameCount * 0.01)) {
        fill(255)
      } else {
        fill(0)
      }
      noStroke()
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(randNums.length * 0.5) + 0.5) * boardSize * (1 / steps) * 0.85, windowHeight * 0.5)
      rect(0, 0, boardSize * (1 / steps), boardSize * (0.2 + 0.1 * sin(Math.PI * (j % 2) + tan(frameCount * 0.01) + i * (1 / steps) * Math.PI * 2)))
      pop()
      pop()
    }
  }

  randNums = []
  for (var i = 0; i < steps; i++) {
    randNums.push(Math.random())
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
